Contenu:

ArgMan.java est une interface à laquelle votre implementation du ArgMan devrait (idéalement) se conformer.

ArgManStub.java est une classe concrète qui implémente l'interface ArgMan, mais ne fait aucun traitement réel.

ExempleUtilisation.java est un exemple de programme (fictif) qui utiliserait un ArgMan.

ArgManTest[1-3].java est une suite de tests permettant de vérifier qu'un ArgMan fonctionne correctement.



Commentaires:

bla bla bla bla bla

Vous devriez tout d'abord créer un nouveau projet Netbeans ou Eclipse à partir de sources existantes (soit les dossiers Source et Tests).

Ensuite, vous devrez rajouter une nouvelle classe qui va hériter de ArgMan.java et implémenter les traitements nécessaires pour être conforme aux exigences décrites dans les spécifications. Pour vous donner une idée d'où commencer, vous pouvez vous baser sur la classe ArgManStub (faites-en une copie).

Quand vous serez prêts à tester votre implémentation (plus tôt que tard!), n'oubliez pas de modifier le contenu de ExempleUtilisation.java et de ArgManTest.java pour instancier votre propre version concrète de ArgMan, et non un ArgManStub!

Souvenez-vous aussi que tous ces fichiers ne sont que des suggestions; Vous êtes libres d'implémenter ArgMan comme vous le souhaitez, quitte à tout refaire depuis zéro.

bla bla bla
