
// "ArgMan" n'est qu'une interface, dont votre implémentation devra hériter.
// Au besoin, si vous le pensez nécessaire, vous pouvez y apporter des modifications.
public interface ArgMan {
    public void loadArgs(String[] args);

    public int getInt(char paramName);
    
    public boolean getBoolean(char paramName);
    
    public String getString(char paramName);
    
}